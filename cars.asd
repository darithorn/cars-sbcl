(defsystem "cars"
  :description "cars - a robot simulation"
  :version "0.0.1"
  :author "Aeron Avery <aeron.avery@cwu.edu>"
  :license "GPLv3"
  :depends-on (:sdl2)
  :pathname "src"
  :serial t
  :components ((:file "vec2")
	       (:file "main")))

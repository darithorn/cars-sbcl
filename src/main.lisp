(require :sdl2)

(defstruct ray
  (id)
  (origin)
  (dir)
  (max-range)
  (current)
  (step-size)
  (active)
  (hit))
(defun new-ray (id origin dir max-range)
  (make-ray
   :id id
   :origin origin
   :dir dir
   :max-range max-range
   :current origin
   :step-size 10
   :active t
   :hit nil))

(defun reset-ray (self origin dir)
  (setf (ray-origin self) origin)
  (setf (ray-dir self) dir)
  (setf (ray-current self) (ray-origin self))
  (setf (ray-active self) t)
  (setf (ray-hit self) nil)
  self)

(defun ray-distance (self)
  (vec2-dist (ray-origin self) (ray-current self)))

(defun ray-step (self dt)
  ; unless the ray is further away from origin than the max range
  (when (ray-active self)
    (if (> (ray-distance self) (ray-max-range self))
	(progn
	  (setf (ray-active self) nil))
	(setf (ray-current self) (vec2-add (ray-current self)
					   (vec2-mul (ray-dir self)
						     (* dt (ray-step-size self))))))))

(defun ray-collision (self)
  (setf (ray-hit self) t)
  (setf (ray-active self) nil))

(defun draw-ray (self renderer)
  (let ((pos-int (vec2->integer (ray-current self)))
	(origin-int (vec2->integer (ray-origin self))))
    (sdl2:set-render-draw-color renderer 255 0 0 255)
    (sdl2:render-draw-line renderer
			   (vec2-int-x origin-int) (vec2-int-y origin-int)
			   (vec2-int-x pos-int) (vec2-int-y pos-int))))

(defstruct circle-obstacle
  (position)
  (radius))

(defun obstacle-contains (self pos)
  (let ((dist (vec2-dist pos (circle-obstacle-position self))))
    (< dist (circle-obstacle-radius self))))

(defstruct physics-world (rays) (obstacles))

(defun new-physics-world ()
  (make-physics-world
   :rays (list)
   :obstacles (list)))

(defun add-ray (self ray)
  (setf (physics-world-rays self) (cons ray (physics-world-rays self))))

(defun contains-ray (self id)
  (position-if (lambda (ray)
		 (= (ray-id ray) id))
	       (physics-world-rays self)))

(defun physics-world-raycast (self id origin dir &optional (max-range 100))
  (let* ((i (contains-ray self id)))
    (if (not (null i))
	(progn
	  (let ((ray (nth i (physics-world-rays self))))
	    (unless (ray-active ray)
	      ; results and reset the ray
	      (let ((done (not (ray-active ray)))
		    (hit (ray-hit ray))
		    (distance (ray-distance ray))
		    (current (ray-current ray)))
		(setf (nth i (physics-world-rays self)) (reset-ray ray origin dir))
		(values done hit distance current)))))
	(progn
	  ; add the new ray to the simulation
	  (add-ray self (new-ray id origin dir max-range))))))

(defmacro raycast (self id origin dir (done hit distance position) &body body)
  `(let ((,done nil)
	 (,hit nil)
	 (,distance 0)
	 (,position nil))
     (setf (values ,done ,hit ,distance ,position)
	   (physics-world-raycast ,self ,id ,origin ,dir))
     ,@body))

(defun physics-world-has-active-rays (self)
  (find-if (lambda (ray) (ray-active ray)) (physics-world-rays self)))

(defun physics-world-simulate (self dt)
  (loop
     (unless (physics-world-has-active-rays self) (return))
     (loop for ray in (physics-world-rays self) do
	  (unless (null ray)
	    ;; test for collisions in obstacles
	    ;; step the rays so they move
	    (ray-step ray dt)
	    (loop for circle in (physics-world-obstacles self) do
		 (when (obstacle-contains circle ray)
		   (ray-collision ray)))))))

(defun draw-physics-world (self renderer)
  (loop for ray in (physics-world-rays self) do
       (draw-ray ray renderer)))

(defstruct motor
  (speed 0.0 :type single-float)
  (pos (make-vec2 :x 0.0 :y 0.0) :type vec2))

(defstruct robot
  (left-motor (make-motor) :type motor)
  (right-motor (make-motor) :type motor)
  (wheel-base 0 :type (signed-byte 64))
  (forward (vec2-forward) :type vec2)
  (right (vec2-right) :type vec2)
  (state 'move :type symbol))
(defun robot-new (w pos &optional (left-speed -5.0) (right-speed 5.0))
  "w -> wheel base
   pos -> position"
  ; (declare (optimize (speed 3)))
  (make-robot
   :left-motor (make-motor :speed left-speed
			   :pos (make-vec2 :x (- (vec2-x pos) (/ w 2.0))
					   :y (vec2-y pos)))
   :right-motor (make-motor :speed right-speed
			    :pos (make-vec2 :x (+ (vec2-x pos) (/ w 2.0))
					    :y (vec2-y pos)))
   :wheel-base w
   :forward (vec2-forward)
   :right (vec2-right)
   :state 'move))

(defun robot-move (self dt)
  (declare (optimize (speed 3)
		     (safety 0))
	   (type robot self)
	   (type single-float dt))
  ; quick accessors to motor speeds
  (let ((left-motor-speed (* (motor-speed (robot-left-motor self)) dt))
	(right-motor-speed (* (motor-speed (robot-right-motor self)) dt)))
    (declare (type single-float left-motor-speed)
	     (type single-float right-motor-speed))
    ; handle special case of motors being the same speed
    ; because if they are it makes w infinite
    (if (not (= left-motor-speed right-motor-speed))
	; calculate w - angle to rotate
	; and r - the radius of the arc
	(let ((w (/ (- right-motor-speed left-motor-speed)
		    (robot-wheel-base self)))
	      (r (/ (* (robot-wheel-base self) left-motor-speed)
		    (- left-motor-speed right-motor-speed))))
	  ; rotate orientation vectors
	  (setf (robot-right self) (vec2-rotate (robot-right self) (* w dt)))
	  (setf (robot-forward self) (vec2-rotate (robot-forward self) (* w dt)))
	  ; create instantaneous center of curvature (icc)
	  (let ((icc (vec2-add (motor-pos (robot-left-motor self))
			       (vec2-mul (robot-right self) r))))
	    ; rotate the motors' position
	    (setf (motor-pos (robot-left-motor self))
		  (vec2-rotate (motor-pos (robot-left-motor self))
			       (* w dt)
			       icc))
	    (setf (motor-pos (robot-right-motor self))
		  (vec2-rotate (motor-pos (robot-right-motor self))
			       (* w dt)
			       icc))))
      ; if motors have same speed
      (progn
	(setf (motor-pos (robot-left-motor self))
	      (vec2-add (vec2-mul (robot-forward self) left-motor-speed)
			(motor-pos (robot-left-motor self))))
	(setf (motor-pos (robot-right-motor self))
	      (vec2-add (vec2-mul (robot-forward self) right-motor-speed)
			(motor-pos (robot-right-motor self))))))))

(defun robot-simulate (self world dt)
;  (robot-move self dt)
  (let ((state (robot-state self)))
    (cond
      ((equal state 'idle)
       ; state is 'idle
       (progn
	 ))
      ((equal state 'move)
       ; state is 'move
       (progn
	 (robot-move self dt)
	 (raycast world 0 (make-vec2 :x 100.0 :y 100.0) (robot-forward self)
		  (done hit distance position)
		  (when done
		    '()))
	 (raycast world 1 (make-vec2 :x 100.0 :y 100.0) (vec2-mul (robot-forward self) -1)
		  (done hit distance position)
		  (when done
		    '())))))))

(defun robot-draw (self renderer)
  (declare (optimize (speed 3)
		     (safety 0))
	   (type robot self))
  ; calculate positions and convert to integers in order to play nice with sdl2
  (let* ((left-motor-pos (motor-pos (robot-left-motor self)))
	 (right-motor-pos (motor-pos (robot-right-motor self)))
	 (center (vec2-midpoint left-motor-pos right-motor-pos))
	 (right (vec2->integer (vec2-add center (vec2-mul (robot-right self) 10))))
	 (forward (vec2->integer (vec2-add center (vec2-mul (robot-forward self) 10))))
	 (left-motor-pos-int (vec2->integer left-motor-pos))
	 (right-motor-pos-int (vec2->integer right-motor-pos))
	 (center-int (vec2->integer center)))
    
    ; draw left motor and right axis
    (sdl2:set-render-draw-color renderer 255 0 0 255)
    (sdl2:render-draw-rect renderer (sdl2:make-rect (vec2-int-x left-motor-pos-int)
						    (vec2-int-y left-motor-pos-int)
						    5 5))
    (sdl2:render-draw-line renderer
			   (vec2-int-x center-int) (vec2-int-y center-int)
			   (vec2-int-x right) (vec2-int-y right))
    ; draw right motor and forward axis
    (sdl2:set-render-draw-color renderer 0 255 0 255)
    (sdl2:render-draw-rect renderer (sdl2:make-rect (vec2-int-x right-motor-pos-int)
						    (vec2-int-y right-motor-pos-int)
						    5 5))
    (sdl2:render-draw-line renderer
			   (vec2-int-x center-int) (vec2-int-y center-int)
			   (vec2-int-x forward) (vec2-int-y forward))))
    
(defvar *total-time* 0.0)
(defvar *dt* 0.01)
(defvar *cur-time* (* (sdl2:get-ticks) 1/1000))
(defvar *accumulator* 0.0)

(defvar robot-car (robot-new 40 (make-vec2 :x 500.0 :y 240.0) 100.0 50.0))
(defvar *world* (new-physics-world))

(defun physics ()
  ; simulate here

  (if (physics-world-has-active-rays *world*)
      (physics-world-simulate *world* *dt*)
      (robot-simulate robot-car *world* *dt*))
;  (physics-world-simulate *world* *dt*)
  
  (setq *accumulator* (- *accumulator* *dt*))
  (setq *total-time* (+ *total-time* *dt*))
  (when (>= *accumulator* *dt*) (physics)))


(defun event-loop (renderer)
  (declare (optimize (safety 0)))
  (sdl2:with-event-loop (:method :poll)
			(:quit () t)
			(:idle ()
			       (let* ((new-time (/ (sdl2:get-ticks) 1000.0))
				      (frame-time (- new-time *cur-time*)))
				 (when (>= frame-time 0.25) (setf frame-time 0.25))
				 (setq *cur-time* new-time)
				 (setq *accumulator* (+ *accumulator* frame-time))
				 
				 (physics)
				 
				 (sdl2:set-render-draw-color renderer 0 0 0 255)
				 (sdl2:render-clear renderer)

				 (robot-draw robot-car renderer)
				 (draw-physics-world *world* renderer)
				 
				 (sdl2:render-present renderer)))))


(sdl2:with-init (:everything)
  (let* ((window (sdl2:create-window :title "cars - sbcl"
				     :x :centered :x :centered
				     :w 640 :h 480
				     :flags '(:shown)))
	 (renderer (sdl2:create-renderer window -1 '(:accelerated))))
    
    (event-loop renderer)
    
    (sdl2:destroy-renderer renderer)
    (sdl2:destroy-window window)))

(defstruct vec2
  (x 0.0 :type single-float)
  (y 0.0 :type single-float))
(defstruct vec2-int
  (x 0 :type integer)
  (y 0 :type integer))

(defun vec2-zero () (make-vec2 :x 0.0 :y 0.0))

(defun vec2-forward () (make-vec2 :x 0.0 :y 1.0))

(defun vec2-right () (make-vec2 :x 1.0 :y 0.0))

(defun vec2-sub (a b)
  (make-vec2
   :x (- (vec2-x a) (vec2-x b))
   :y (- (vec2-y a) (vec2-y b))))

(defun vec2-add (a b)
  (make-vec2
   :x (+ (vec2-x a) (vec2-x b))
   :y (+ (vec2-y a) (vec2-y b))))

(defun vec2-mul (a s)
  (make-vec2
   :x (* (vec2-x a) s)
   :y (* (vec2-y a) s)))

(defun vec2-dot (a b)
  (declare (optimize (speed 3))
	   (type vec2 a b))
  (+ (* (vec2-x a) (vec2-x b))
     (* (vec2-y a) (vec2-y b))))

(defun vec2-dist (a b)
  (sqrt (+ (expt (- (vec2-x b) (vec2-x a)) 2)
	   (expt (- (vec2-y b) (vec2-y a)) 2))))

(defun vec2-mag (a)
  (sqrt (vec2-dot a a)))

(defun vec2-norm (a)
  (declare (optimize (speed 3)
		     (safety 0))
	   (type vec2 a))
  (let ((mag (vec2-mag a)))
    (declare (type single-float mag))
    (make-vec2
     :x (/ (vec2-x a) mag)
     :y (/ (vec2-y a) mag))))

(defun vec2-rotate (v a &optional (o (vec2-zero)))
  (declare (optimize (speed 3)
		     (safety 0))
	   (type vec2 v)
	   (type single-float a)
	   (type vec2 o))
  (setf (vec2-x v) (- (vec2-x v) (vec2-x o)))
  (setf (vec2-y v) (- (vec2-y v) (vec2-y o)))
  (make-vec2
   :x (+ (- (* (vec2-x v) (cos a))
	 (* (vec2-y v) (sin a)))
      (vec2-x o))
   :y (+ (+ (* (vec2-x v) (sin a))
	 (* (vec2-y v) (cos a)))
      (vec2-y o))))

(defun vec2-midpoint (a b)
  (make-vec2
   :x (/ (+ (vec2-x a) (vec2-x b)) 2)
   :y (/ (+ (vec2-y a) (vec2-y b)) 2)))

(defun vec2->integer (a)
  (make-vec2-int
   :x (round (vec2-x a))
   :y (round (vec2-y a))))
